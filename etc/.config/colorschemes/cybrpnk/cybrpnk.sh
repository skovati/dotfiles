#!/usr/bin/env bash 
background='#181818' 
foreground='#d8d8d8' 
color0='#171717' 
color1='#ba423f' 
color2='#94983a' 
color3='#d7b44a' 
color4='#658fa0' 
color5='#b080a4' 
color6='#76ab97' 
color7='#e8e4d9' 
color8='#6e6e6e' 
color9='#ba423f' 
color10='#94983a' 
color11='#d7b44a' 
color12='#658fa0' 
color13='#b080a4' 
color14='#76ab97' 
color15='#e8e4d9'